package myComponents
{
	import com.as3xls.xls.Cell;
	import com.as3xls.xls.ExcelFile;
	import com.as3xls.xls.Sheet;
	
	import flash.net.FileReference;
	import flash.utils.ByteArray;
	import flash.external.ExternalInterface;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ArrayList;
	import mx.collections.GroupingCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.controls.AdvancedDataGrid;
	import mx.controls.Alert;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.formatters.CurrencyFormatter;
	import mx.formatters.DateFormatter;
	import mx.formatters.NumberFormatter;
	
	
	public class ExportDataToExcel
	{
		//	private var fileReference:FileReference;
		private var xls:Class;
		private var sheet:Sheet;
		private var fields:Array = new Array();  
		private var dataProviderCollection:ArrayCollection;
		private var columnList:ArrayCollection;
		private var totalRow:Object;
		private var allTotalRow:Object;
		private var cF:CurrencyFormatter= new CurrencyFormatter();
		private var reportName:String;

		public function ExportDataToExcel(name:String, datagrid:ArrayCollection, columns:ArrayCollection, totalRowArr:Object, allTotalRowArr:Object)
		{
			cF.precision = "2";
			cF.rounding = "nearest";
			cF.useNegativeSign = "false";
			cF.useThousandsSeparator = "false";
			cF.useNegativeSign = "true";
			
			dataProviderCollection = datagrid;
			columnList = new ArrayCollection();
			columnList.addAll(columns);
			if (allTotalRowArr != null)
				columnList.addItem("Total");
			totalRow  = totalRowArr;	
			allTotalRow = allTotalRowArr;
			reportName = name;
		}
		
		public function exportToExcel():void
		{
			sheet = new Sheet();
			if (dataProviderCollection == null && dataProviderCollection.length == 0){
				return;
			}
			//		sortDataArray(dataProviderCollection);
			var rowCount:int =  dataProviderCollection.length;
			//	Alert.show("export =" +rowCount);
			if (allTotalRow != null)
				sheet.resize(rowCount+3, columnList.length); 
			else
				sheet.resize(rowCount+2, columnList.length); 
			
			for(var i:int ;i< columnList.length; i++)
			{
				sheet.setCell(0, i, columnList[i]);
			}
			for(var r:int=0;r<rowCount;r++)
			{
				var record:Object = dataProviderCollection.getItemAt(r);
				/*insert record starting from row no 2 else
				headers will be overwritten*/
				insertRecordInSheet(r+1,sheet,record);
			}
			//insert the total row
			insertRecordInSheet(rowCount+1,sheet,totalRow);
			if (allTotalRow != null)
				insertRecordInSheet(rowCount+2, sheet, allTotalRow);
			var xls:ExcelFile = new ExcelFile();
			xls.sheets.addItem(sheet);
			
			var bytes: ByteArray = xls.saveToByteArray();
			var fr:FileReference = new FileReference();
			var currentDate:Date = new Date();
			fr.save(bytes, reportName + "_" + currentDate.toDateString() +".xls");
		}
		
		private function insertRecordInSheet(row:int,sheet:Sheet,item:Object):void
		{
			for(var c:int=0; c< columnList.length;c++)
			{
				var columnName: String = columnList[c];
				var value:Object ;
				if (item != null && item[columnName] != null)
					value = item[columnName];
				else
					value = "";
				//	if (c > 1)
				//		value = cF.format(value);
				sheet.setCell(row,c,  value);
				
				//	if (row == 1)
				//		Alert.show(columnList[c] + " = " +record[columnList[c]]);
			}
		} 
		
	}
}