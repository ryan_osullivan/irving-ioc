package com.forestandthetrees.controls.adgFooter
{
import mx.controls.AdvancedDataGrid;

public class FooterAdvancedDataGrid extends AdvancedDataGrid
{

	public function FooterAdvancedDataGrid()
	{
		super();
	}

    protected var footer:AdvancedDataGridFooter;

	protected var footerHeight:int = 22;

    override protected function createChildren():void
    {
        super.createChildren();

        if (!footer)
        {
            footer = new AdvancedDataGridFooter();
            footer.styleName = this;
            addChild(footer);
        }
    }

    override protected function adjustListContent(unscaledWidth:Number = -1,
                                       unscaledHeight:Number = -1):void
    {
		super.adjustListContent(unscaledWidth, unscaledHeight);

		listContent.setActualSize(listContent.width, listContent.height - footerHeight);
		
		footer.setActualSize(listContent.width, footerHeight);
		footer.move(listContent.x, listContent.y + listContent.height + 1);
	}

	override public function invalidateDisplayList():void
	{
		super.invalidateDisplayList();
		if (footer)
			footer.invalidateDisplayList();
	}
}

}