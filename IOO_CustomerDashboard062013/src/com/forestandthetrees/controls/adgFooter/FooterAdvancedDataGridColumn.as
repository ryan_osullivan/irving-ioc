package com.forestandthetrees.controls.adgFooter
{
import mx.controls.advancedDataGridClasses.AdvancedDataGridColumn;


[DefaultProperty("footerColumn")]

public class FooterAdvancedDataGridColumn extends AdvancedDataGridColumn
{

	public function FooterAdvancedDataGridColumn()
	{
		super();
	}
	public var footerColumn:AdvancedDataGridColumn = 
		new AdvancedDataGridColumn();

}

}