package com.forestandthetrees.controls.dgFooter
{
import mx.controls.DataGrid;
import mx.controls.dataGridClasses.DataGridColumn;

public class FooterDataGrid extends DataGrid
{

	public function FooterDataGrid()
	{
		super();
	}

    protected var footer:DataGridFooter;

	protected var footerHeight:int = 22;

    override protected function createChildren():void
    {
        super.createChildren();

        if (!footer)
        {
            footer = new DataGridFooter();
            footer.styleName = this;
            addChild(footer);
        }
    }

    override protected function adjustListContent(unscaledWidth:Number = -1,
                                       unscaledHeight:Number = -1):void
    {
		super.adjustListContent(unscaledWidth, unscaledHeight);

		listContent.setActualSize(listContent.width, listContent.height - footerHeight);
		//this deals w/ having locked columns - it's handled differently in
		//the dg and the adg
		footer.setActualSize(listContent.width+listContent.x, footerHeight);
		footer.move(1, listContent.y + listContent.height + 1);
	}

	override public function invalidateDisplayList():void
	{
		super.invalidateDisplayList();
		if (footer)
			footer.invalidateDisplayList();
	}
}

}